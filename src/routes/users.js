// Remember to follow https://www.notion.so/24g/API-ecec568a919f47b5b2ab40b880873866
const express = require('express');
const userController = require('../controllers/users');

const router = express.Router();

router.get('/:user', userController.getReverse);

module.exports = router;
