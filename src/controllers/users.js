function reverseString(str) {
  const splitString = str.split("");
  const reverseArray = splitString.reverse(); 
  const joinArray = reverseArray.join("");

  return joinArray;
}


module.exports.getReverse = async function get(req, res) {
  const user = req.params.user;
  const userReverse = reverseString(user);
  
  res.send(userReverse);
};