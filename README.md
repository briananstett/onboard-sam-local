# Onboarding Exercise
Very simple SAM project to demonstrate how routing works.

## Objectives
* Run the SAM project locally as is.
* Add two more routes. One more `/users` route and a new `/onboarding` route.

## Usage
```
$ npm install --prefix ./src
$ sam local start-api
```