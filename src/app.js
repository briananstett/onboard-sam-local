const express = require('express'); // https://www.npmjs.com/package/express
const morgan = require('morgan'); // https://www.npmjs.com/package/morgan // Makes apache like logs
const cors = require('cors'); // https://www.npmjs.com/package/cors

const router = require('./routes/index');

const app = express();

app.use('*', cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use(morgan('combined', {
  skip: (req) => ['/_healthz', '/favicon.ico'].includes(req.url),
}));

app.use(router);

module.exports = app;
